package com.hotcomm.psql.ibatis;

import org.apache.ibatis.session.SqlSession;

import com.hotcomm.psql.ibatis.mapper.MessageMapper;
import com.hotcomm.psql.ibatis.vo.MessageVO;

public class PsqlHelper {
	
	public static void main(String[] args) {
		SqlSession session = IbatisConfig.sqlSessionFactory.openSession();
		MessageMapper mapper = session.getMapper(MessageMapper.class);
		MessageVO vo = new MessageVO();
		vo.setMac("0001");
		vo.setMessage("admin002");
		vo.setChannel("u001");
		mapper.saveMessage(vo);;
		System.out.println("提交新增成功");
		session.commit();
		session.close();
	}
	
}
