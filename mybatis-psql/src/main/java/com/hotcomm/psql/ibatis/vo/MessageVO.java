package com.hotcomm.psql.ibatis.vo;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@lombok.Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class MessageVO {
	
	private String mac;
	private String message;
	private String channel;
	
}
