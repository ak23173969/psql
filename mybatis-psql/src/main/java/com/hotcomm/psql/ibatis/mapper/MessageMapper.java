package com.hotcomm.psql.ibatis.mapper;

import org.apache.ibatis.annotations.Param;

import com.hotcomm.psql.ibatis.vo.MessageVO;

public interface MessageMapper {
	
	public void saveMessage(@Param("message")MessageVO message);
	
}
