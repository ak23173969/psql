package com.hotcomm.boot.psql.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hotcomm.boot.psql.bean.MessageVO;

public interface MessageMapper {
	
	public void insert(@Param("message")MessageVO message);
	
	public void count(MessageVO message);
	
	public void udpate(MessageVO message);
	
	public void del(String mac);
	
	public List<MessageVO> queryList();
}
