package com.hotcomm.boot.psql.service;

import com.hotcomm.boot.psql.bean.MessageVO;

public interface MessageService {
	
	public void addMessage(MessageVO vo);
	
}
