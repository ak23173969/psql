package com.hotcomm.boot.psql.web.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hotcomm.boot.psql.bean.MessageVO;
import com.hotcomm.boot.psql.service.MessageService;

@RestController
@RequestMapping("/message")
public class MessageController {
	
	@Autowired
	MessageService messageService;
	
	@RequestMapping("/list")
	public List<MessageVO> list(){
		List<MessageVO> list = new ArrayList<>();
		MessageVO vo = new MessageVO();
		vo.setChannel("019101");
		vo.setMac("000000331");
		vo.setMessage("33231019fj19111");
		list.add(vo);
		return list;
	}
	
	@RequestMapping("/add")
	public Map<String, Object> add(MessageVO message){
		Map<String, Object> result = new HashMap<>();
		messageService.addMessage(message);
		result.put("code", 1);
		return result;
	}
	
}
