package com.hotcomm.boot.psql;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages= {"com.hotcomm.boot.psql.mapper"})
public class PsqlRunnr {

	public static void main(String[] args) {
		SpringApplication.run(PsqlRunnr.class, args);
	}
}
