package com.hotcomm.boot.psql.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotcomm.boot.psql.bean.MessageVO;
import com.hotcomm.boot.psql.mapper.MessageMapper;

@Service
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	MessageMapper messageMapper;

	@Override
	public void addMessage(MessageVO vo) {
		messageMapper.insert(vo);
	}
	
	
}
