package com.hotcomm.test;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@lombok.Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class Data {
	
	private String mac;
	private String message;
	public Data(String mac, String message) {
		super();
		this.mac = mac;
		this.message = message;
	}
	
}
